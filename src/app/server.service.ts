import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ServerService {
	
	constructor(private Http:Http) {}

	storeServers(servers:any[]) {
		const headers = new Headers({'Content-Type':'application/json'});
		/*
		 * this would not work cannot send headers
		 */
		// return this.Http.post('https://udemy-ng-http-javier.firebaseio.com/data.json',servers, {headers:headers});
		// return this.Http.post('https://udemy-ng-http-javier.firebaseio.com/data.json',servers);
		return this.Http.put('https://udemy-ng-http-javier.firebaseio.com/data.json',servers);
	}
	getServers() {
		/*
		 * use this one to test the catch()
		 */
		// return this.Http.get('https://udemy-ng-http-javier.firebaseio.com/data')
		return this.Http.get('https://udemy-ng-http-javier.firebaseio.com/data.json')
		.map(
			(response:Response)=>{
				const data = response.json();
				for( const server of data ) {
					server.name = 'FETCHED_' + server.name;
				}
				return data;
			}
		)
		.catch(
			(error:Response)=>{
				return Observable.throw(error);
			}
		)
		;
	}
	getAppName() {
		return this.Http.get('https://udemy-ng-http-javier.firebaseio.com/appName.json')
		.map(
			(Response:Response)=>{
				return Response.json();
			}
		)
		;
	}
}