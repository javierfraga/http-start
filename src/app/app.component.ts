import { Component } from '@angular/core';
import { Response } from '@angular/http';
import {ServerService} from './server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appName = this.ServerService.getAppName();
  servers = [
    {
      name: 'Testserver',
      capacity: 10,
      id: this.generateId()
    },
    {
      name: 'Liveserver',
      capacity: 100,
      id: this.generateId()
    }
  ];
  constructor(private ServerService:ServerService ){}
  onAddServer(name: string) {
    this.servers.push({
      name: name,
      capacity: 50,
      id: this.generateId()
    });
  }
  save() {
    this.ServerService.storeServers(this.servers)
    .subscribe(
      (response)=>console.log(response),
      (error)=>console.log(error)
    );
  }
  get() {
    this.ServerService.getServers()
    .subscribe(
      (data:any[])=>{
        // const data = response.json();
        console.log(data);
        this.servers = data;
      },
      (error)=>console.log(error)
    );
  }
  private generateId() {
    return Math.round(Math.random() * 10000);
  }
}
